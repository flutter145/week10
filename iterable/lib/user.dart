bool anyUserUnder18(Iterable<User> users) {
  return users.any((user) => user.age < 18);
}

bool everyUserOver13(Iterable<User> users) {
  return users.every((user) => user.age >13);
}

Iterable<User> filterOutUnder21(Iterable<User> users) {
  return users.where((user) => user.age >= 21);
}

Iterable<User> filterShortName(Iterable<User> users) {
  return users.where((user) => user.name.length <= 3);
}

Iterable<String> getNameAndAge(Iterable<User> users) {
  return users.map((user) => '${user.name} is ${user.age}');
}
void main() {
  var users = [
    User(name: 'AQVE', age: 14),
    User(name: 'BVEQWVE', age: 15),
    User(name: 'CVEQ', age: 16),
    User(name: 'DVE', age: 21),
    User(name: 'E', age: 25)
  ];
  if(anyUserUnder18(users)) {
    print('Have any user under 18');
  }
  if(everyUserOver13(users)) {
    print('Have every user over 13');
  }

  var ageMoreThan21Users = filterOutUnder21(users);
  for(var user in ageMoreThan21Users) {
    print(user.toString());
  }

  var  shortNameUsers = filterShortName(users);
  for(var user in shortNameUsers) {
    print(user.toString());
  }

  var nameAndage = getNameAndAge(users);
  for(var user in nameAndage) {
    print(user);
  }
}

class User {
  String name;
  int age;
  User({required this.name, required this.age});
  @override
  String toString() {
    return '$name,$age';
  }
}
